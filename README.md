# ML DevOps
The aim of this projects is to provide a thorough guide on the following:
- [x] Set up the OpenShift to the local and VM.
- [x] Use the OpenShift to deploy the service.
- [x] Creating the Machine Learning (ML) API via FAST API.
- [x] Building up ML service by Docker.
- [x] Using OpenShift to deploy ML services.

## Setup
### Prerequisites: 
####Virtualisation Tools
Use [brew](https://brew.sh/) to install hypervisor packages below for virtualisation purpose to run Minishift cluster on local machine. 

#### Option 1: HyperKit
```bash
brew install hyperkit;
brew install docker-machine-driver-hyperkit;
brew link --overwrite hyperkit;
brew link --overwrite docker-machine-driver-hyperkit
```

Run the following scripts to alter permissions so that minishift script can be executed without error.

```bash
sudo chown root:wheel /usr/local/bin/docker-machine-driver-hyperkit && sudo chmod u+s /usr/local/bin/docker-machine-driver-hyperkit;

sudo chown root:wheel /usr/local/bin/hyperkit && sudo chmod u+s /usr/local/bin/hyperkit;

```

#### Option 2: Virtualbox
```bash
brew install virtualbox
```

### Install OpenShift VM
#### Recommended: CodeReady Container OpenShift (CRC) -OpenShift4
Sign up a RedHat account and download the prebuilt CRC package (circa 2GB) from [here](https://developers.redhat.com/products/codeready-containers/overview).

Download the macOS version alongside with the pull secret.

Add the OC tools into your .bashrc or .zshrc.
```
export PATH="$HOME/.crc/bin/oc:$PATH"
```

#### Alternative: MiniShift on Mac - OpenShift3
<strong>Recommended:</strong> Install Minishift via Brew:
``` bash
brew install minishift;

echo 'export PATH=$PATH:/usr/local/bin/minishift' >> ~/.bashrc;

echo 'export PATH="~/.minishift/cache/oc/v3.11.0/darwin:$PATH"' >> ~/.bashrc;

source bashrc
```

<strong>Or</strong> Install Minishift Manually:
``` bash
wget https://github.com/minishift/minishift/releases/download/v1.34.2/minishift-1.34.2-darwin-amd64.tgz;

tar -xvzf minishift-1.34.2-darwin-amd64.tgz && rm minishift-1.34.2-darwin-amd64.tgz;

mv minishift-1.34.2-darwin-amd64/minishift /usr/local/bin/minishift && rm -rf minishift-1.34.2-darwin-amd64;

echo 'export PATH=$PATH:/usr/local/bin/minishift' >> ~/.bashrc;

echo 'export PATH="~/.minishift/cache/oc/v3.11.0/darwin:$PATH"' >> ~/.bashrc;

source bashrc
```

## Usage
### WebConsole

Start the CRC VM. You can customise the VM specs by changing the parameters. Replace `hyperkit` with `virtualbox` if you're using Virtualbox.

``` bash
crc start --vm-driver hyperkit --disk-size 60GB --cpus 4
```

It will takes awhile for the cluster to be loaded and it will output an url for you to access the web console for OpenShift. 



<strong>Example terminal output for success setup:</strong>
```console
user@localhost:~$ minishift start --vm-driver hyperkit 

.
.
.
Login to server ...
Creating initial project "myproject" ...
Server Information ...
OpenShift server started.

The server is accessible via web console at:
    https://192.168.64.3:8443/console

You are logged in as:
    User:     developer
    Password: <any value>

To login as administrator:
    oc login -u system:admin

```

### CLI: OC Tools
#### 1. Login
Admin Login:
```bash
oc login -u admin -p <any value>
```
Developer Login:
```bash
oc login -u developer -p <any value>
```
#### 2. Create New Project
``` bash
oc new-project <projectname>
```

#### 3. Select Project
``` bash
oc project <projectname>
```

#### 4. Create New Application
``` bash
oc new-app -l name=<appname> --build-env GIT_SSL_NO_VERIFY=false <repo_url>
```

## Uninstallation
Delete the Minishift cluster.
``` bash
minishift delete;

sudo rm /usr/local/bin/minishift;

rm -rf ~/.minishift
```



